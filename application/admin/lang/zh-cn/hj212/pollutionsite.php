<?php

return [
    'Deviceid'        => '设备Id',
    'SiteName'            => '站点名称',
    'Address'        => '站点地址',
    'Lon'             => '经度',
    'Lat'             => '纬度',
    'Industrial_park' => '园区名称',
    'Contact'         => '厂家联系方式',
    'Created_at'      => '创建时间',
    'Updated_at'      => '更新时间'
];
