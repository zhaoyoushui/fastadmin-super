<?php

return [
    'Code'      => '监测因子',
    'Name'      => '监测因子名称',
    'Measures'  => '缺省计量单位(浓度)',
    'Emissions' => '缺省计量单位(排放量)',
    'Type'      => '缺省数据类型(浓度)'
];

