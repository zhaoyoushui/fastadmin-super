<?php

return [
    'Code'     => '监测因子',
    'Alarm_min'  => '报警最小值',
    'Alarm_max'  => '报警最大值',
    'Avg_min'   => '监测时间内最小平均值',
    'Avg_max'   => '监测时间内最大平均值',
    'Created_at' => '创建时间',
    'Updated_at' => '更新时间',
    'Alarm'=>'报警阈值'
];
