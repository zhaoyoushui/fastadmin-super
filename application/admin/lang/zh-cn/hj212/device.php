<?php

return [
    'Device_code' => '设备号',
    'Device_pwd'  => '设备连接密码',
    'Created_at' => '创建时间',
    'SiteName'            => '站点名称',
    'bindSite'            => '绑定站点名称',
    'Contact' => '联系方式'
];
