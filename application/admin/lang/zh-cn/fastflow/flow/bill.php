<?php

return [
    'Id'         => 'ID',
    'Bill_name'  => '单据名称',
    'Bill_table' => '单据表名',
    'Controller' => '控制器',
    'Fileds'     => '字段数据',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间',
    'Search icon' => '搜索图标',
	'BillAuth'    => '权限配置'
];
