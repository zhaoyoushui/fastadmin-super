<?php

return [
    'Id'                => '线程ID',
    'Flow_id'           => '流程ID',
    'FlowName'          => '流程',
    'Bill'              => '单据表',
    'Bill_id'           => '单据ID',
    'Bill_name'         => '单据名称',
    'Bill_status'       => '单据状态',
    'Bill_status 0'     => '已删除',
    'Bill_status 1'     => '正常',
    'Status'            => '流程状态',
    'Status 1'          => '运行',
    'Status 2'          => '结束',
    'Status 3'          => '终止'
];
