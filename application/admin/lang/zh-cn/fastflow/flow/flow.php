<?php

return [
    'id'             => '编号',
    'Name'           => '名称',
    'Bill'           => '单据表',
    'Description'    => '描述',
    'Status 0'       => '禁用',
    'Status 1'       => '启用',
    'Createuser_id'  => '创建用户ID',
    'Createuser_name'=> '创建用户',
    'Createtime'     => '创建时间',
    'Designer'       => '流程设计',
    'Remark'         => '备注',
    'Bills'          => '单据表',

];
