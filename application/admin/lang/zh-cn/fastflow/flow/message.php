<?php

return [
    'Id'       => 'ID',
    'Name'     => '名称',
    'Key'      => '键名',
    'Config'   => '参数',
    'Templet'  => '模板',
    'Status'   => '状态',
    'Status 0' => '禁用',
    'Status 1' => '启用'
];
