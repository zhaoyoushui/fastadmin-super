<?php

return [
    'Chooseflow'     => '选择流程',
    'Startcontent'   => '发起意见',
    'Checkcontent'   => '审批意见',
    'Preview'        => '流程预览',
    'SelectWorker'   => '为后续步骤动态指定审批人员（分组）',
    'File'           => '资料',
    'Filename'       => '资料名称',
    'SelectSignWorker'  => '选择会签人员（分组）',
    'BackAuth'       => '驳回权限',
    'SignAuth'       => '会签权限',
    'BackTo'         => '驳回步骤',
];
