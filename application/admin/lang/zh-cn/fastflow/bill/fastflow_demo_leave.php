<?php

return [
    'Id'        => 'ID',
    'Admin_id'  => '创建用户',
    'Status'    => '状态',
    'Status 0'  => '待发起',
    'Status 1'  => '流程中',
    'Status 2'  => '通过',
    'Status 3'  => '终止',
    'Type'      => '请假类型',
    'Type 1'    => '休假',
    'Type 2'    => '事假',
    'Type 3'    => '因公',
    'Days'      => '请假天数',
    'Reason'    => '请假理由',
    'Starttime' => '请假时间',
    'Endtime'   => '到假时间',
    'Addr'      => '外出地址',
    'Phone'     => '联系方式',
    'File'      => '证明材料'
];
