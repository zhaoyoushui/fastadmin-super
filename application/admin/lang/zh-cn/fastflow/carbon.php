<?php

return [
    'Id'         => '编号',
    'Sender_id'  => '抄送人ID',
    'Sendtime'   => '抄送时间',
    'Is_read'    => '是否已读',
    'Is_read 0'  => '未读',
    'Is_read 1'  => '已读',
    'Bill_name'  => '单据',
    'Bill_id'    => '单据ID',
    'Bill'       => '单据',
    'DisplayAll' => '显示全部',
    'Sender_id'  => '抄送人ID',
    'Sender_name' => '抄送人',
    'Sendtime'   => '抄送时间',
    'View'       => '查阅',
];
